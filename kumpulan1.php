<?php

function randStr($length = 32) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ`~!@#$%^&*()-=_+';
	$charlen = strlen($characters) - 1;
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charlen)];
    }
    return $randomString;
}

function hashPass($password, $salt) {
	$firsthash = hash('sha256', $password);
	$hash = hash('sha256', $firsthash . $salt);
	return $hash;
}

function copyright($year = 'auto') {
	if(intval($year) == 'auto') $year = date('Y');
	if(intval($year) == date('Y')) echo intval($year);
	if(intval($year) < date('Y')) echo intval($year) . '-' . date('Y');
	if(intval($year) > date('Y')) echo date('Y');
}